@include('layers.header')
    <h2>Task:</h2>
    <p>Create a simple "URL shortener" PHP web application.</p>
    <p>The main (perhaps only) page would be simple form, where you put in the complete URL, and after submitting the website will give you shortlink URL.</p>
    <p>Then, if you use this short URL, you will be redirected to the original long URL</p>
    <p>For example, when I put in address "https://medium.com/@nykolas.z/dns-resolvers-performance-compared-cloudflare-x-google-x-quad9-x-opendns-149e803734e5" and it will give me URL "http://localhost/d8aj4s"  When i open another tab and put in URL "http://localhost/d8aj4s", i should be redirected to URL "https://medium.com/@nykolas.z/dns-resolvers-performance-compared-cloudflare-x-google-x-quad9-x-opendns-149e803734e5"</p>
    <p>Store data in SQL database. Use your favorite PHP framework. The app needs to be ready for production (reasonably optimized, safe). Push your source to repository.</p>

    <hr>
    <h2>Form for create short link:</h2>
    <form action="{{ route('send-form') }}" method="post" class="form-inline justify-content-center">
        <div class="form-group row">
            <div class="col-sm-12">
                {{ csrf_field() }}
                <input type="text" placeholder="Enter your url" name="url" class="form-control" />
                <button class="btn btn-primary">encode</button>
            </div>
        </div>
    </form>
@include('layers.footer')