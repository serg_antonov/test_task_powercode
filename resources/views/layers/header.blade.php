<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Test task for Powercode</title>

        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    </head>
    <body class="container-fluid">
        <header>
            <nav>
                <a href="{{ route('home') }}">Home</a>
                <a href="{{ route('list') }}">All urls</a>
            </nav>
        </header>
        <section>
