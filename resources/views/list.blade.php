@include('layers.header')
    <table class="table">
        <thead>
            <tr>
                <th>№</th>
                <th>Short URL</th>
                <th>URL</th>
                <th>Added</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($list as $item)
                <tr class="inline">
                    <td>
                        {{ $item->id }}
                    </td>
                    <td>
                        <input type="text" value="{{ route('decode-url', $item->hash) }}" class="form-control" />
                    </td>
                    <td>
                        <a href="{{ $item->url }}">{{ $item->url }}</a>
                    </td>
                    <td>
                        {{ $item->created_at }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@include('layers.footer')