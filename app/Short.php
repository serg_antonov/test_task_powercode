<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Short extends Model
{
    protected $table = 'urls';
    /**
     * Make short link
     *
     * @param string $url Source url
     * @return string
     */
    public function encode(string $url) : string {
        if( !filter_var($url, FILTER_VALIDATE_URL) ) {
            return '';
        }

        // if url exists - return exists hash
        $without_http_protocol = str_replace('http://', '', $url);
        $without_http_protocol = str_replace('https://', '', $without_http_protocol);
        $exists_url = $this->where('url', 'like', '%'.$without_http_protocol)->first();
        if( $exists_url !== null ) {
            return route('decode-url', $exists_url->hash);
        }

        $result = '';
        $unique_id = uniqid();

        $this->hash = $unique_id;
        $this->url = $url;
        if( $this->save() ){
            $result = route('decode-url', $unique_id);
        }

        return $result;
    }

    /**
     * Decode shorted url to original url
     *
     * @param string $hash Hash of shorted url
     * @return string
     */
    public function decode(string $hash) : string {
        $original_url = $this->where('hash', $hash)->first();
        if( $original_url !== null ){
            return $original_url->url;
        }

        return '';
    }
}
