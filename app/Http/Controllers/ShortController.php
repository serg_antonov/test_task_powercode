<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Short;

class ShortController extends Controller
{

    /**
     * Redirect to home if user requested this page form browser
     *
     * @return void
     */
    public function index(){
        return response()->redirectTo('/', 302);
    }

    /**
     * Make short link
     *
     * @param Request $request Source url
     * @return void
     */
    public function encode(Request $request) {

        if(!$request->has('url')){
            return response()->redirectTo('/', 400);
        }

        $link = new Short();
        $short_link = $link->encode($request->get('url'));
        if( !empty($short_link) ){
            return view('encode', ['short'=>'You shorted link: <input type="text" value="'.$short_link.'" class="form-control" />', 'error'=>null]);
        }

        return view('encode', ['error'=>'Error. Please, try again']);
    }

    /**
     * Redirect to original link
     *
     * @param string $hash Short link
     * @return void
     */
    public function decode(string $hash) {
        $link = new Short();
        $original_link = $link->decode($hash);
        if( !empty($original_link) ) {
            return redirect($original_link, 302);
        }

        //@TODO: return 404
        return view('errors.404', ['url'=>request()->url()]);
    }

    /**
     * List all urls & hashes
     *
     * @return void
     */
    public function list() {
        return view('list', ['list'=>Short::all(['id', 'hash', 'url', 'created_at'])]);
    }

}
