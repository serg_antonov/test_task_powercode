# Deploy
Create empty database

Set your database host, name, user and password to .env file. Short description of variables below.

* DB_HOST host of database (for example DB_HOST=localhost)
* DB_DATABASE name of database (for example DB_DATABASE=test_task_powercode)
* DB_USERNAME database user name (for example DB_USERNAME=admin)
* DB_PASSWORD password for database user (for example DB_PASSWORD=qwerty)

After enter credential to your database to .env file save this file and go to project folder and run
`php artisan migration`
in your comand line.

For publish error page run
`php artisan vendor:publish --tag=laravel-errors`
in your comand line.

