FROM composer:1.9 as build

WORKDIR /app
COPY . /app
RUN composer install

FROM php:7.2-apache

EXPOSE 80
COPY --from=build /app /app
COPY .deploy/apache2/vhost.conf /etc/apache2/sites-available/000-default.conf
RUN docker-php-ext-install mbstring pdo pdo_mysql \
    && chown -R www-data:www-data /app \
    && a2enmod rewrite
